# gradle-wrapper

当前仓库不再维护，请使用[阿里云官方镜像](https://help.aliyun.com/document_detail/618142.html)。

#### 介绍

Gradle工程使用wrapper管理简单、易移植。个人不建议在全局配置一个特定版本的Gradle。因此我将Gradle生成的wrapper拿出来作为初始工程。而非官方推荐的方式。